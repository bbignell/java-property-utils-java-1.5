/**
 * This package provides a Java utility for typed retrieval of 
 * {@code java.util.Properties} as boolean, integer, floating point, string, 
 * enum and URL values.
 *
 * <p>System requirements:
 *
 * <ul>
 *     <li>Java 1.5 or later (for generics and enum support)
 * </ul>
 *
 * @author Vladimir Dzhuvinov
 */
package com.thetransactioncompany.util;
