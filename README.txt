Java Property Utility

Copyright (c) Vladimir Dzhuvinov, 2010 - 2013

README

This package provides a Java utility for typed retrieval of java.util.Properties
as boolean, integer, floating point, string and enum values.


Package requirements:

	* Java 1.5+


[EOF]
